import {defineConfig} from 'vite'
import {resolve} from 'path'
import libInjectCss from './build-tools/libInjectCss';

export default defineConfig({
  esbuild: {
    minify: true
  },
  assetsInclude: [
    'templates/*.tmpl.html',
  ],
  plugins: [
    libInjectCss(),
    {
      name: "configure-response-headers",
      configureServer: (server) => {
        server.middlewares.use((_req, res, next) => {
          res.setHeader("Access-Control-Allow-Origin", "*");
          next();
        });
      },
    },
  ],
  build: {
    target: ['chrome64', 'edge79', 'firefox62', 'safari11.1'],
    minify: 'terser',
    lib: {
      entry: resolve(__dirname, 'src/index.ts'),
      name: 'Glossa',
      formats: ['es']
    },
    rollupOptions: {
      plugins: [],
    }
  }
})