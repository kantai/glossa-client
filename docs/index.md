# Glossa client

[Glossa](https://glossa.cc) is a service that allows website owners to integrate user comments
and discussion threads that can be attached to individual parts of a particular page. The 
glossa-client package contains the integration code, that needs to be included into the HTML
code of the pages in order to allow users to create and show the comments.


The package is written in TypeScript, contains a set of HTML templates and SCSS style definitions
and is compiled into a single JavaScript module that can easily be integrated into existing
HTML pages. For style it uses the excellent [picocss](https://picocss.com) CSS library with some
minor changes/additions, that can be found in the package's style directory. 
[vite](https://vitejs.dev/) is used as a build took and internally the library depends on
[petite-vue](https://github.com/vuejs/petite-vue), [marked](https://github.com/markedjs/marked)
and [dompurify](https://github.com/cure53/DOMPurify).

The client is developmed by [Kantai GmbH](https://kant.ai) and licensed under
[MIT License](https://gitlab.com/kantai/glossa-client/-/blob/mainline/LICENSE).

### Interested in integration of glossa into your web page?
Take a look at our [Getting started documentation](./getting_started/).

[//]: # (### Want to learn more how you can adapt the client to your needs?)

[//]: # (Read through our [configuration]&#40;./configuration/&#41; chapter to understand what can be changed.)

[//]: # ()
[//]: # (### Do you want the client to blend in naturally with your own design?)

[//]: # (Take a look at our information about [styling]&#40;./styling/&#41; using custom CSS.)

### You have other questions?
Feel free to reach out to us. We provide a form for requesting support at
[https://admin.glossa.cc/support](https://admin.glossa.cc/support) or via email to
[support@glossa.cc](mailto:support@glossa.cc).
Additional support channels can be found on our [documentation hub](https://docs.kant.ai)
on [https://docs.kant.ai](https://docs.kant.ai).
