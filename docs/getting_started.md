# Getting started

## Add glossa-client to your webpage

Within your HTML page load the latest version from the official CDN:

```html
<html>
   ...
   <script type="module">
     import { initGlossa } from "https://cdn.glossa.cc/glossa-client.es.js"
     initGlossa('LOCAL-DUMMY-API-KEY');
   </script>
   ...
  </body>
</html>
```

We use the dummy API key `'LOCAL-DUMMY-API-KEY'` here, that allows to store the
comments in the browsers local storage. It enables you to work on the integration
and test it without the need to set up an account.

## Glossa user interface

### Control panel
Glossa's user interface is quite simple. It's control panel is shown in light gray at a
fixed position on the right side of the embedding page. Hovering about its buttons highlights
them:

control panel             |  with highlighted button
:------------------------:|:-------------------------:
![](./control_panel.png)  |  ![](./control_panel_hover.png)

### Creating a new thread

One can create a new thread by selecting the content on the page, that the new thread should
be about. When something is selected, glossa shows a new symbol next to the selection:

Select to create new thread     |
:------------------------------:|
![](./new_thread_selection.png) |


Clicking on the symbol opens a new modal dialog that allows to add a comment to the selected
part of the page. The dialog allows to write a comment and start a new discussion thread. To
allow for more expressiveness, the comments are written in
[markdown](https://daringfireball.net/projects/markdown/syntax).

Comment dialog            |
:------------------------:|
![](./comment_dialog.png) |

with written comment       |
:-------------------------:|
![](./comment_written.png) |


### Displaying other comments

All threads that were created on a specific page are marked by individual thread markers
that show up in grey next to the content that was commented.

Thread marker            |
:-----------------------:|
![](./thread_marker.png) |

Hovering above the thread marker, highlights the content that was commented in this thread:

Thread marker with highlight   |
:-----------------------------:|
![](./thread_marker_hover.png) |

The whole discussion thread is shown by clicking the individual thread marker.

## Where to go from here?

- Create an account and setup a plan to store and share your comments. For this visit
  [https://admin.glossa.cc](https://admin.glossa.cc), sign up and create a new domain.
  Afterwards replace the `'LOCAL-DUMMY-API-KEY'` with a real API key so that the comments
  are actually stored and visible to everyone.

[//]: # (- Configure glossa according to your wishes. More information about this can be found in)

[//]: # (  our [configuration]&#40;../configuration/&#41; section.)

[//]: # (- Adapt glossa to the style of your pages to make it blend in. A short description about the)

[//]: # (  possibility to change its appearance using CSS is written down [here]&#40;../styling/&#41;.)
