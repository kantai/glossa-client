import { marked } from "marked";
import DOMPurify from 'dompurify';

import { CommentData, ThreadData, ThreadListData } from "./datatypes"
import { glossa_root} from "./root";
import { highlight } from "./highlight";
import { settings } from "./settings";

import threadTemplateString from "../templates/thread.tmpl.html?raw"

glossa_root.insertAdjacentHTML('beforeend', threadTemplateString)


function Thread(
  threadlist: ThreadListData,
  setCurrentThread: (a: ThreadListData | null, b: ThreadData | null) => void,
  saveCommentToThread: (a: ThreadData, b: CommentData) => Promise<void>
) {
  const dateFormat = new Intl.DateTimeFormat('en', { timeStyle: 'short', dateStyle: 'short' })
  return {
    $template: `#glossa-thread-template`,
    thread: threadlist.current,
    threadlist: threadlist,
    newComment: <CommentData> {text: "", author: null, timestamp: null},
    isPreview: false,
    isBusy: false,
    isCommentValid: false,
    showUser: false,
    name: <string | null> '',

    compiledMarkdown(text: string) {
      return DOMPurify.sanitize(marked(text))
    },
    formatTimestamp(timestamp: number) {
      // Timestamp is number of seconds, javascript uses milliseconds
      return dateFormat.format(timestamp*1000);
    },
    toggleUser() {
      this.showUser = ! this.showUser
    },
    inputName(event: KeyboardEvent) {
      if (event.key === "Enter") {
        event.preventDefault();
        settings.setName(this.name!)
        this.toggleUser()
      }
    },
    togglePreview() {
      this.isPreview = ! this.isPreview
    },
    addComment() {
      if(this.name !== null) {
        this.newComment.author = this.name.trim()
      }
      const commentToStore = Object.assign({}, this.newComment)
      this.isBusy = true
      saveCommentToThread(this.thread!, commentToStore)
        .then(() => {
          this.newComment.text = ''
          this.isPreview = false
        })
        .finally(() => {
          this.isBusy = false
        })
    },
    newCommentTextChanged() {
      this.isCommentValid = this.newComment.text.trim().length > 4
    },
    previousThread() {
      if(this.threadlist.currentIndex !== null) {
        const previousIndex = this.threadlist.currentIndex - 1
        if (previousIndex >= 0) {
          this.threadlist.currentIndex = previousIndex
          this.threadlist.current = this.threadlist.threads[previousIndex]
          this.thread = this.threadlist.current
          highlight(this.thread)
        }
      }
    },
    nextThread() {
      if(this.threadlist.currentIndex !== null) {
        const nextIndex = this.threadlist.currentIndex + 1
        if (nextIndex < this.threadlist.threads.length) {
          this.threadlist.currentIndex = nextIndex
          this.threadlist.current = this.threadlist.threads[nextIndex]
          this.thread = this.threadlist.current
          highlight(this.thread)
        }
      }
    },
    copyToClipboard(thread: ThreadData, comment: CommentData, element: HTMLElement) {
      const id = '#glossa:' + thread.id + ':' + comment.timestamp
      const url = new URL(window.location.href)
      url.hash = id
      navigator.clipboard.writeText(url.toString()).then(() => {
        element.setAttribute('data-tooltip', 'Link copied to clipboard')
        const delay = setInterval(() => {
          element.removeAttribute('data-tooltip')
          clearInterval(delay)
        }, 2000)
      })
    },
    close() {
      setCurrentThread(this.threadlist, null)
    },
    mounted() {
      if (this.thread) {
        highlight(this.thread)
      }
      this.name = settings.getName()
    }
  }
}


export { Thread }
