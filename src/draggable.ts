import { DirectiveContext } from "petite-vue/dist/types/directives";


function draggableDirective(ctx: DirectiveContext) {
  let dragging = false
  let draggableRoot = (ctx.el as HTMLElement)
  let draggableHandle = draggableRoot
  if(ctx.exp) {
    const draggableChild = ctx.el.querySelector(ctx.exp)
    if(draggableChild === null) {
      console.warn("No child element found for selector "+ctx.exp)
    } else {
      draggableHandle = draggableChild as HTMLElement
    }
  }
  const pos = {x:0, y:0}

  let x = 0;
  let y = 0;
  const storedX = window.sessionStorage.getItem('thread.pos.x')
  const storedY = window.sessionStorage.getItem('thread.pos.y')
  if(storedX) {
    x = parseInt(storedX, 10)
  }
  if(storedY) {
    y = parseInt(storedY, 10)
  }
  draggableRoot.style.left = String(x)+'px';
  draggableRoot.style.top = String(y)+'px';

  function startDragging(event: MouseEvent) {
    dragging = true
    let currentX: number = 0
    if(draggableRoot.style.left) {
      currentX = parseInt(draggableRoot.style.left, 10)
    }
    let currentY: number = 0
    if(draggableRoot.style.top) {
      currentY = parseInt(draggableRoot.style.top, 10)
    }
    pos.x = event.pageX - currentX
    pos.y = event.pageY - currentY
    window.addEventListener('mousemove', dragDialog)
    window.addEventListener('mouseup', stopDragging)
  }
  function stopDragging() {
    dragging = false
    window.removeEventListener('mousemove', dragDialog)
    let currentX: number = 0
    if(draggableRoot.style.left) {
      currentX = parseInt(draggableRoot.style.left, 10)
    }
    let currentY: number = 0
    if(draggableRoot.style.top) {
      currentY = parseInt(draggableRoot.style.top, 10)
    }
    window.sessionStorage.setItem('thread.pos.x', String(currentX))
    window.sessionStorage.setItem('thread.pos.y', String(currentY))
  }
  function dragDialog(event: MouseEvent) {
    if(dragging) {
      const x = event.pageX - pos.x
      const y = event.pageY - pos.y
      draggableRoot.style.left = String(x)+'px';
      draggableRoot.style.top = String(y)+'px';
    }
  }

  draggableHandle.addEventListener('mousedown', startDragging)

  return () => {
    draggableHandle.removeEventListener('mousedown', startDragging)
  }
}

export { draggableDirective }