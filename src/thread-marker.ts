import { glossa_root } from "./root"

import { ThreadData, ThreadListData } from "./datatypes";
import { highlight } from "./highlight";


import templateString from "../templates/thread-marker.tmpl.html?raw";


glossa_root.insertAdjacentHTML('beforeend', templateString)


function ThreadMarker(
  thread: ThreadData,
  setCurrentThread: (a: ThreadListData | null, b: ThreadData) => void
) {
  let rangeRect: DOMRect = new DOMRect(-1000, -1000)
  if(thread.range !== undefined) {
    rangeRect = thread.range.getBoundingClientRect()
  }

  return {
    $template: '#glossa-thread-marker-template',
    highlightElement: null,
    x: rangeRect.left+window.pageXOffset,
    y: rangeRect.top+window.pageYOffset,
    updateThread(selection: Selection) {
      let rangeRect: DOMRect = new DOMRect(-1000, -1000)
      if(thread.range !== undefined) {
        rangeRect = thread.range.getBoundingClientRect()
      }
      this.x = rangeRect.left+window.pageXOffset
      this.y = rangeRect.top+window.pageYOffset
    },
    toggleThread(event: Event) {
      event.preventDefault()
      setCurrentThread(null, thread)
    },
    ignore(event: Event) {
      event.preventDefault()
    },
    highlightRange() {
      highlight(thread)
    },
    hideRange() {
      // @ts-ignore
      highlight(this.anchoredThreads.current)
    }
  }
}

export { ThreadMarker }
