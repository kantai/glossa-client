import { glossa_root } from "./root"
import { ThreadListData } from "./datatypes";
import templateString from "../templates/control-panel.tmpl.html?raw";


glossa_root.insertAdjacentHTML('beforeend', templateString)


function ControlPanel(orphanedThreads: ThreadListData) {

  return {
    $template: '#glossa-control-panel-template',
    orphanedThreads: orphanedThreads,
    get hasOrphanedThreads(): boolean {
      return this.orphanedThreads.threads.length > 0
    },
    showOrphanedThreads() {
      this.orphanedThreads.current = this.orphanedThreads.threads[0]
    }
  }
}

export { ControlPanel }
