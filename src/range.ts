import { finder } from "@medv/finder";

import { Subject, Position } from './datatypes'

function nodeWithOffsetToJson(node: Node, offset: number): Position {
  let element: Element | null;
  let childNodeIndex = null;
  if(node.nodeType !== Node.ELEMENT_NODE) {
    element = node.parentElement
    let index = 0;
    while(true) {
      if(node.previousSibling)
        node = node.previousSibling;
      else
        break;
      ++index;
    }
    childNodeIndex = index
  } else {
    element = node as Element
  }
  return {
    selector: finder(element!),
    childNodeIndex: childNodeIndex,
    offset: offset
  }
}

function nodeAndOffsetFromJson(obj: Position): [Node, number] {
  let node = document.querySelector(obj.selector)
  if(node !== null && obj.childNodeIndex !== null) {
    node = node.childNodes[obj.childNodeIndex] as Element
  }
  return [node!, obj.offset]
}


function rangeToSubject(range: Range) {
  return {
    start: nodeWithOffsetToJson(range.startContainer, range.startOffset),
    end: nodeWithOffsetToJson(range.endContainer, range.endOffset),
    text: range.toString(),
  }
}


function rangeFromSubject(subject: Subject) {
  try {
    const range = new Range()
    const [startNode, startOffset] = nodeAndOffsetFromJson(subject.start)
    const [endNode, endOffset] = nodeAndOffsetFromJson(subject.end)
    if (startNode === null || endNode === null) {
      return null
    }
    range.setStart(startNode, startOffset)
    range.setEnd(endNode, endOffset)
    return range
  } catch(DOMException) {
    return null
  }
}


export { rangeToSubject, rangeFromSubject }