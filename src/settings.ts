

class Settings {
  prefix = 'glossa.settings.'

  setName(name: string) {
    localStorage.setItem(this.prefix+'name', name)
  }

  getName() {
    return localStorage.getItem(this.prefix+'name')
  }

  setShowMarkers(showMarkers: boolean) {
    localStorage.setItem(this.prefix+'showMarkers', String(showMarkers))
  }

  getShowMarkers() {
    const showMarkersString = localStorage.getItem(this.prefix+'showMarkers')
    if(showMarkersString === 'false') {
      return false
    }
    return true
  }
}

const settings = new Settings()

export { settings }