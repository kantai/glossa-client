import { createApp, nextTick, reactive } from "petite-vue";

import {CommentData, ThreadData, ThreadListData} from "./datatypes";
import { ControlPanel } from './control-panel'
import { Thread } from './thread'
import { ThreadMarker} from "./thread-marker";
import { rangeFromSubject, rangeToSubject } from "./range";
import { highlight } from "./highlight";
import { disabledDirective } from "./disabled";
import { draggableDirective } from "./draggable";
import { selectionDirective } from "./selection-handler";
import { settings } from "./settings";
import { createStorage, Storage, UpdateHandler } from "./storage";


const url = new URL(window.location.href)

function sortThreadsByRange(threads: any[]) {
  threads.sort((a, b) => a.range.compareBoundaryPoints(Range.START_TO_START, b.range))
}

function locateThreadInDocument(thread: ThreadData){
  const range = rangeFromSubject(thread.subject)
  if (range && range.toString() == thread.subject.text) {
    thread.range = range
    return true
  } else {
    return false
  }
}

function initGlossa(apiKey: string, domain: string = url.hostname, collection: string = url.pathname) {

  const app = reactive({
    anchoredThreads: <ThreadListData> {
      threads: <Array<ThreadData>> [],
      current: null,
      currentIndex: null,
    },
    orphanedThreads: <ThreadListData> {
      threads: <Array<ThreadData>> [],
      current: null,
      currentIndex: null
    },
    linkedCommentId: <string | null> null,
    disabled: false,
    showMarkers: settings.getShowMarkers(),
    selection: window.getSelection(),
    storage: <Storage | null> null,
    _newThread: <ThreadData | null> null,
    _highlightElement: null,

    get newThread() {
      if(this.selection && this.selection.rangeCount && this.selection.toString()) {
        const range = this.selection.getRangeAt(0)
        if(! this._newThread) {
          this._newThread = <ThreadData> {
            comments: [],
            name: undefined,
            range: range,
            subject: rangeToSubject(range),
          }
        }
      } else {
        this._newThread = null
      }
      return this._newThread
    },

    setNewThreadSubject(selection: Selection) {
      if(this._newThread) {
        const range = selection.getRangeAt(0)
        console.log("In new thread: " + range.toString())
        this._newThread.range = range
        this._newThread.subject = rangeToSubject(range)
      }
    },

    saveCommentToThread(thread: ThreadData, comment: CommentData) : Promise<void> {

      return this.storage!.saveCommentToThread(thread, comment)
        .then((result: [ThreadData, CommentData]) => {
          const [newThread, newComment] = result
          comment.timestamp = newComment.timestamp
          comment.needsApproval = newComment.needsApproval
          thread.comments.push(comment)
          const oldIndex = this.anchoredThreads.threads.findIndex((t: ThreadData) => t.id == newThread.id)
          if(oldIndex == -1) {
            const oldIndex = this.orphanedThreads.threads.findIndex((t: ThreadData) => t.id == newThread.id)
            if(oldIndex == -1) {
              thread.id = newThread.id
              thread.timestamp = newThread.timestamp
              this.anchoredThreads.threads.push(thread)
              sortThreadsByRange(this.anchoredThreads.threads)
              this.setCurrentThread(this.anchoredThreads, thread)
            }
          }
        })
    },

    toggleMarkers() {
      this.showMarkers = ! this.showMarkers
      settings.setShowMarkers(this.showMarkers)
    },
    setCurrentThread(threadlist: ThreadListData, thread: ThreadData) {
      if(threadlist === null) {
        threadlist = this.anchoredThreads
      }
      if(thread && ! thread.id) {
        threadlist.current = null
      }
      nextTick(() => {
        threadlist.current = thread
        threadlist.currentIndex = threadlist.threads.indexOf(thread)
        if(threadlist === this.anchoredThreads) {
          highlight(thread)
        }
      })
    },

    ControlPanel,
    ThreadMarker,
    Thread,
  })

  const updateHandler: UpdateHandler = {
    onNewThread(thread: ThreadData): void {
      if(locateThreadInDocument(thread)) {
        if(! app.anchoredThreads.threads.find((t) => t.id == thread.id)) {
          // @ts-ignore
          app.anchoredThreads.threads.push(thread)
          sortThreadsByRange(app.anchoredThreads.threads);
        }
      } else {
        if(! app.orphanedThreads.threads.find((t) => t.id == thread.id)) {
          // @ts-ignore
          app.orphanedThreads.threads.push(thread)
          app.orphanedThreads.threads.sort((a, b) => a.timestamp! - b.timestamp!)
        }
      }
    },
    onNewComment(comment: CommentData): void {
      const threadId = comment.threadId
      let threadIndex = app.anchoredThreads.threads.findIndex((t) => t.id == threadId)
      if(threadIndex > -1) {
        const commentIndex = app.anchoredThreads.threads[threadIndex].comments.findIndex((c) => c.timestamp == comment.timestamp)
        if(commentIndex > -1) {
          app.anchoredThreads.threads[threadIndex].comments[commentIndex] = comment
        } else {
          app.anchoredThreads.threads[threadIndex].comments.push(comment)
        }
      } else {
        threadIndex = app.orphanedThreads.threads.findIndex((t) => t.id == threadId)
        if(threadIndex > -1) {
          const commentIndex = app.orphanedThreads.threads[threadIndex].comments.findIndex((c) => c.timestamp == comment.timestamp)
          if(commentIndex > -1) {
            app.orphanedThreads.threads[threadIndex].comments[commentIndex] = comment
          } else {
            app.orphanedThreads.threads[threadIndex].comments.push(comment)
          }
        } else {
          console.error("New comment for unknown thread with id ", threadId)
        }
      }
    }
  }

  createStorage(
    apiKey, domain, collection, updateHandler,apiKey == 'LOCAL-DUMMY-API-KEY',
  ).then((storage: Storage) => {

    app.storage = storage

    const showLinkedComment = () => {
      const url = new URL(window.location.href)
      if(url.hash) {
        const parts = url.hash.split(':')
        if(parts.length == 3 && parts[0] == '#glossa') {
          const threadId = parts[1]
          const commentId = parts[2]
          let linkedThread = app.anchoredThreads.threads.find(t => t.id == threadId)
          let threadList = app.anchoredThreads
          if(! linkedThread) {
            linkedThread = app.orphanedThreads.threads.find(t => t.id == threadId)
            threadList = app.orphanedThreads
          }
          if(linkedThread) {
            // @ts-ignore
            app.setCurrentThread(threadList, linkedThread)
            app.linkedCommentId = commentId
          }
        }
      }
    }

    const loadPromise = storage.loadThreads()

    loadPromise.then((allThreads: Array<ThreadData>) => {
      const threads: Array<ThreadData> = []
      const orphanedThreads: Array<ThreadData> = []
      allThreads.forEach((thread) => {
        if(locateThreadInDocument(thread)) {
          threads.push(thread)
        } else {
          orphanedThreads.push(thread)
        }
      })
      sortThreadsByRange(threads);
      orphanedThreads.sort((a, b) => a.timestamp! - b.timestamp!)
      // @ts-ignore
      app.anchoredThreads.threads = threads
      // @ts-ignore
      app.orphanedThreads.threads = orphanedThreads

      showLinkedComment()
    }).catch(function(err: Error) {
      if(err.message == 'Forbidden') {
        console.error("Forbidden to access comments, disabling glossa.")
        app.disabled = true
      } else {
        console.error(err)
      }
    });

    createApp(app).directive(
      'selection', selectionDirective
    ).directive(
      'disabled', disabledDirective
    ).directive(
      'draggable', draggableDirective
    ).mount('#glossa')

    window.addEventListener('hashchange', showLinkedComment)
  })
}

export { initGlossa }
