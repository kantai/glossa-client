import { DirectiveContext } from "petite-vue/dist/types/directives";


function disabledDirective(ctx: DirectiveContext) {
  ctx.effect(() => {
    if(ctx.get()) {
      if(! ctx.el.hasAttribute("disabled")) {
        ctx.el.setAttribute("disabled", "disabled")
      }
    } else {
      if(ctx.el.hasAttribute("disabled")) {
        ctx.el.removeAttribute("disabled")
      }
    }
  })
  return () => {
    if(ctx.el.hasAttribute("disabled")) {
      ctx.el.removeAttribute("disabled")
    }
  }
}

export { disabledDirective }