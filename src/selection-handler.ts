import {DirectiveContext} from "petite-vue/dist/types/directives";


function selectionDirective(ctx: DirectiveContext) {
  let selectionDelay: number | null = null;

  const handler = function () {
    if (selectionDelay) {
      window.clearTimeout(selectionDelay);
    }
    selectionDelay = window.setTimeout(() => {
      ctx.ctx.scope[ctx.exp] = document.getSelection()
      selectionDelay = null;
    }, 100);
  }

  window.document.addEventListener('selectionchange', handler)

  return () => {
    window.document.removeEventListener('selectionchange', handler)
  }
}
export { selectionDirective }