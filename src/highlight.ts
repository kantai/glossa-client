import {glossa_root} from "./root";

interface Highlightable {
  range?: Range;
}

let highlightElement: HTMLElement | null = null;

function highlight(thread: Highlightable | null) {
  if(highlightElement) {
    highlightElement.remove()
    highlightElement = null
  }
  if(thread && thread.range instanceof Range) {
    const highlightRect = thread.range.getBoundingClientRect()
    const newElement = document.createElement("div");
    newElement.className = 'glossa-highlight'
    newElement.style.top = String(highlightRect.top+window.pageYOffset)+'px'
    newElement.style.left = String(highlightRect.left+window.pageXOffset)+'px'
    newElement.style.width = String(highlightRect.width)+'px'
    newElement.style.height = String(highlightRect.height)+'px'
    highlightElement = glossa_root.appendChild(newElement)
    if(
      highlightRect.top <= 100 ||
      highlightRect.left <= 100 ||
      highlightRect.bottom >= (window.innerHeight || document.documentElement.clientHeight) - 100 ||
      highlightRect.right >= (window.innerWidth || document.documentElement.clientWidth) - 100
    ) {
      highlightElement.scrollIntoView({block: "center", inline: "nearest"})
    }
  }
}

export { highlight, Highlightable }
