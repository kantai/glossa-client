import templateString from "../templates/root.html?raw";
import "../style/glossa.scss"

function createGlossaRoot() : HTMLElement {
  document.body.insertAdjacentHTML('afterbegin', templateString)
  const root = document.getElementById("glossa")
  return root!
}

const glossa_root: HTMLElement = createGlossaRoot()

export { glossa_root }
