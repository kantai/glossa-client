import { nextTick } from "petite-vue";
import {CommentData, ThreadData} from "./datatypes";

const module_url = import.meta.url

async function digestMessage(message: string, length=64) {
  const msgUint8 = new TextEncoder().encode(message);                           // encode as (utf-8) Uint8Array
  const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);           // hash the message
  const hashArray = Array.from(new Uint8Array(hashBuffer));                     // convert buffer to byte array
  const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join(''); // convert bytes to hex string
  return hashHex.substring(0, length);
}

interface Storage {
  saveCommentToThread(thread: ThreadData, comment: CommentData): Promise<[ThreadData, CommentData]>;
  loadThreads(): Promise<Array<ThreadData>>;
}


interface UpdateHandler {
  onNewThread(thread: ThreadData): void;
  onNewComment(comment: CommentData): void;
}


function createStorage(
  apiKey: string,
  domain: string,
  collection: string,
  updateHandler: UpdateHandler | null = null,
  useLocalStorage: boolean = false): Promise<Storage> {

  return new Promise((resolve) => {
    digestMessage(apiKey+":"+domain+":"+collection, 32)
      .then((collectionKey) => {

        if(useLocalStorage) {
          const storage = new LocalStorage(collectionKey)
          resolve(storage)
        } else {
          let backendUrl = 'https://api.glossa.cc/client/'
          let rtuUrl = 'wss://rtu.glossa.cc/v1/'
          if(module_url.indexOf('cdn-dev.glossa.cc') > -1) {
            backendUrl = 'https://api-dev.glossa.cc/client/'
            rtuUrl = 'wss://rtu-dev.glossa.cc/v1/'
          } else if(module_url.indexOf('cdn-test.glossa.cc') > -1) {
            backendUrl = 'https://api-test.glossa.cc/client/'
            rtuUrl = 'wss://rtu-test.glossa.cc/v1/'
          } else if(module_url.indexOf('localhost') > -1) {
            backendUrl = 'http://localhost:5000/client/'
            rtuUrl = 'ws://localhost:4001/'
          //   backendUrl = 'https://api-dev.glossa.cc/client/'
          //   rtuUrl = 'wss://rtu-dev.glossa.cc/v1/'
          }
          const urlParams = new URL(window.location.href).searchParams
          if(urlParams.has('glossa-backend')) {
            backendUrl = urlParams.get('glossa-backend')!
          }
          if(urlParams.has('glossa-rtu')) {
            rtuUrl = urlParams.get('glossa-rtu')!
          }
          const storage = new BackendStorage(
            backendUrl, apiKey, collectionKey, domain, collection,
          )
          if(updateHandler) {
            const socket = new WebSocket(rtuUrl + `?apiKey=${apiKey}&collectionKey=${collectionKey}`)
            socket.addEventListener('message', function (event) {
              const message = JSON.parse(event.data)
              if('newComment' in message) {
                updateHandler.onNewComment(message.newComment)
              } else if('newThread' in message) {
                updateHandler.onNewThread(message.newThread)
              } else {
                console.warn('Unknown message received: ', event.data);
              }
            });
          }
          resolve(storage)
        }
      });
  })
}


class LocalStorage implements Storage {
  collectionKey: string;
  threads: Array<any>;

  constructor(collectionKey: string) {
    this.collectionKey = collectionKey
    this.threads = []
  }

  saveCommentToThread(thread: ThreadData, comment: CommentData) : Promise<[ThreadData, CommentData]> {
    return new Promise((resolve) => {
      const newThread = Object.assign({}, thread)
      if(! newThread.id) {
        newThread.timestamp = Date.now() / 1000
        newThread.id = String(newThread.timestamp)
      }
      const oldIndex = this.threads.findIndex((t) => t.id == newThread.id)
      if(oldIndex > -1) {
        this.threads.splice(oldIndex, 1)
      }
      const newComment = Object.assign({}, comment)
      newComment.timestamp = Date.now() / 1000
      newThread.comments = Object.assign([], newThread.comments);

      this.threads.push(newThread)
      newThread.comments.push(newComment)

      const threadsJson = JSON.stringify(this.threads)
      window.localStorage.setItem(this.collectionKey, threadsJson)
      resolve([newThread, newComment])
    })
  }

  loadThreads() : Promise<Array<ThreadData>> {
    return new Promise((resolve, reject) => {
      nextTick(() => {
        const threadsJson = window.localStorage.getItem(this.collectionKey)
        if (threadsJson) {
          const allThreads = JSON.parse(threadsJson)
          resolve(allThreads)
          this.threads = allThreads
        } else {
          reject("No threads found.")
        }
      })
    })
  }
}


class BackendStorage implements Storage  {
  apiKey: string;
  collectionUrl: string;
  collectionKey: string;
  domain: string;
  collection: string;

  constructor(backendUrl: string, apiKey: string, collectionKey: string, domain: string, collection: string) {
    this.apiKey = apiKey
    this.collectionKey = collectionKey
    this.collectionUrl = backendUrl+'collections/'+collectionKey
    this.domain = domain
    this.collection = collection
  }

  saveCommentToThread(thread: ThreadData, comment: CommentData) : Promise<[ThreadData, CommentData]> {
    return new Promise((resolve, reject) => {
      if(thread.id) {
        resolve(thread)
      } else {
        this.createThread(thread).then((newThread) => {
          resolve(newThread)
        }).catch((error) => {
          reject(error)
        })
      }
    }).then((newThread) => {
      return this.createComment(newThread as ThreadData, comment)
    })
  }

  loadThreads() : Promise<Array<ThreadData>> {
    return fetch(this.collectionUrl, {
      method : "GET",
      mode: 'cors',
      headers: {
        'Accept': 'application/json',
        'X-Api-Key': this.apiKey,
      },
    }).then((response) => {
      if (response.status >= 200 && response.status < 300) {
        return response.json()
      } else if (response.status == 403) {
        throw Error('Forbidden')
      } else {
        throw Error(response.statusText)
      }
    });
  }

  createThread(thread: ThreadData){
    const newThread = Object.assign({}, thread)
    newThread.collection = this.collection
    delete newThread.range
    return fetch(this.collectionUrl+'/threads', {
      method : "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-Api-Key': this.apiKey,
      },
      body : JSON.stringify(newThread)
    }).then((response) => {
      if (response.status >= 200 && response.status < 300) {
        return response.json()
      } else {
        throw Error(response.statusText)
      }
    });
  }

  createComment(thread: ThreadData, comment: CommentData) : Promise<[ThreadData, CommentData]> {
    return fetch(this.collectionUrl+'/threads/'+thread.id+'/comments', {
      method : "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-Api-Key': this.apiKey,
      },
      body : JSON.stringify(comment)
    }).then((response) => {
      if (response.status >= 200 && response.status < 300) {
        return response.json()
      } else {
        throw Error(response.statusText)
      }
    }).then((newComment) => {
      return [thread, newComment]
    });
  }
}

export { createStorage, Storage, UpdateHandler }
