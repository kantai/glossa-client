import {Highlightable} from "./highlight";

interface Position {
  selector: string;
  childNodeIndex: number | null;
  offset: number;
}

interface Subject {
  start: Position;
  end: Position;
  text: string;
}

interface CommentData {
  text: string;
  author: string | null;
  timestamp?: number | null;
  needsApproval?: boolean | null;
  threadId?: string | null;
}

interface ThreadData extends Highlightable {
  id?: string;
  name?: string;
  comments: Array<CommentData>;
  timestamp?: number;
  collection?: string;
  subject: Subject;
}

interface ThreadListData {
  threads: Array<ThreadData>;
  current: ThreadData | null;
  currentIndex: number | null;
}

export { CommentData, ThreadData, ThreadListData, Position, Subject }